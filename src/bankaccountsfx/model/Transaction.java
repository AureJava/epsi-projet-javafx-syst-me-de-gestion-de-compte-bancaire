/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccountsfx.model;

import bankaccountsfx.utils.FileUtils;
import static bankaccountsfx.utils.MessageUtils.showError;
import java.io.UnsupportedEncodingException;
import java.text.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose J. Pardines Garcia
 */
public class Transaction {
    
    String Numero_Compte;
    Date date;
    String description;
    float quantite;
    
    /**
     * Init a Transaction object with all of its data
     * @param Numero_Compte
     * @param date
     * @param description
     * @param quantite 
     */
    public Transaction( 
        String Numero_Compte,
        Date date,
        String description,
        float quantite
    ){
        
        this.Numero_Compte = Numero_Compte;
        this.date = date;
        this.description = description;
        this.quantite = quantite;
        
    }
    
    /**
     * Save a new account number in Transaction object
     * @param newAccountNumber 
     */
    
    public void setNumero_Compte( String newNumero_Compte ){ this.Numero_Compte = newNumero_Compte; }
    
    /**
     * Get the account number from Transaction object
     * @return String
     */
    
    public String getNumero_Compte() { return this.Numero_Compte; }
    
    /**
     * Save a new transaction data in Transaction object
     * @param newDate 
     */
    
    public void setDate( Date newDate ){ this.date = newDate; }
    
    /**
     * Get the transaction data in format (dd/MM/yyyy) from Transaction object
     * @return String
     */
    
    public String getDate() { 
        
        DateFormat dateFormat = new SimpleDateFormat( "dd/MM/yyyy" );
        return dateFormat.format( this.date ); 
        
    } 
    
    /**
     * Save a new transaction description in Transaction object
     * @param newDescription 
     */
    
    public void setDescription( String newDescription ){ this.description = newDescription; }
    
    /**
     * Get the transaction description from Transaction object
     * @return String
     */
    
    public String getDescription() { return this.description; }
    
    /**
     * Save a new transaction amount in Transaction object
     * @param newAmount 
     */
    
    public void setquantite( Float newquantite ){ this.quantite = newquantite; }
    
    /**
     * Get the transaction amount from Transaction object
     * @return Float
     */
    
    public Float getquantite() { return this.quantite; }
    
    @Override
    public String toString(){ 
                    
       return this.Numero_Compte + 
               ";" + 
               getDate() + 
               ";" + 
               this.description + 
               ";" + 
               this.quantite;
       
    }
    
}
