/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccountsfx.model;

import bankaccountsfx.utils.FileUtils;
import static bankaccountsfx.utils.MessageUtils.showError;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose J. Pardines Garcia
 */
public class Compte 
{
    
    String Numero_Compte;
    String Proprietaire;

    /**
     * Init a Account object with all of its data
     * @param NumeroCompte
     * @param owner 
     */
    public Compte( 
        String Numero_Compte,
        String Proprietaire 
    ){
        
        this.Numero_Compte = Numero_Compte;
        this.Proprietaire = Proprietaire;
        
    }
    
    /**
     * Save a new account number in Account object
     * @param newAccountNumber 
     */
    public void setNumero_Compte( String newNumero_Compte ){ this.Numero_Compte = newNumero_Compte; }
    
    /**
     * Get the account number from Account object
     * @return String
     */
    
    public String getNumero_Compte() 
    { 
        return this.Numero_Compte; 
    } 
    
    /**
     * Save a new owner in Account object
     * @param newOwner 
     */
    
    public void setProprietaire( String newProprietaire ){ this.Proprietaire = newProprietaire; }
    
    /**
     * Get the owner from Account object
     * @return String
     */
    
    public String getProprietaire() { return this.Proprietaire; }
    
    @Override
    public String toString(){ 
                    
       return this.Numero_Compte + 
               ";" + 
               this.Proprietaire;
       
    }
    
}
