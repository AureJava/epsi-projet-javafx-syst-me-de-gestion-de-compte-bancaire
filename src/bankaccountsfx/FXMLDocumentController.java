/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankaccountsfx;

import bankaccountsfx.model.*;
import bankaccountsfx.utils.*;
import static bankaccountsfx.utils.FileUtils.*;
import static bankaccountsfx.utils.MessageUtils.*;
import java.io.*;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;
import java.util.logging.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jose J. Pardines Garcia
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private TextField accountNumber;
    @FXML
    private TextField owner;
    @FXML
    private Label totalSalary;
    @FXML
    private Button btnAccountAdd;
    @FXML
    private ComboBox<Compte> cmbAccount;
    @FXML
    private TextField transactionDescription;
    @FXML
    private TextField transactionAmount;
    @FXML
    private DatePicker transactionDate;
    @FXML
    private Button btnTransactionAdd;
    @FXML
    private Button btnTransactionChart;
    @FXML
    private ComboBox<String> cmbFilter;
    @FXML
    private TableView<Transaction> tableTransactions;
    private List<String> params;
    public List<Compte> listAccounts;
    public List<Transaction> listTransactions;
    @FXML
    private Label textTotal;
    private PieChart blanceChart;
    @FXML
    private AnchorPane panePie;
    @FXML
    private AnchorPane paneData;
    @FXML
    private VBox vBox;
    @FXML
    private PieChart blanceChartPositive;
    @FXML
    private PieChart blanceChartNegative;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Vous avez cliqué!");
        label.setText("Bonjour monde!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            /**
             * Styles
             */
            btnAccountAdd.setStyle("-fx-text-fill: white; -fx-background-color: #000000");
            btnTransactionAdd.setStyle("-fx-text-fill: white; -fx-background-color: #000000");
            btnTransactionChart.setStyle("-fx-text-fill: white; -fx-background-color: #000000");
            
            btnTransactionAdd.setDisable( true );
            btnTransactionChart.setDisable( true );
            transactionDate.setDisable( true );
            transactionDescription.setDisable( true );
            transactionAmount.setDisable( true );
            cmbFilter.setDisable( true );
            
            /**
             * Add filters
             */
            cmbFilter.getItems().add( "Filtre" );
            cmbFilter.getItems().add( "Date" );
            cmbFilter.getItems().add( "Description" );
            cmbFilter.getItems().add( "Quantite" );
            cmbFilter.getItems().add( "Revenue" );
            cmbFilter.getItems().add( "Depenses" );
            cmbFilter.getSelectionModel().selectFirst();
            
            /**
             * Listener: Shows all of Transactions filtered for an account in a table
             */
            cmbFilter.setOnAction((ActionEvent e) -> {
                
                try{
                     if( ( cmbAccount.getValue() == null || 
                        cmbAccount.getValue().toString().trim().equals( "" ) ) &&
                        !cmbFilter.getValue().equals( "Filter" ) ) {
                    
                        showError( "Erreur!", "Un Compte doit être sélectionné." );

                    }else{
                        
                        filterTransactions();
                         
                    }
                    
                }catch(IOException | NullPointerException ex){
                     Logger
                            .getLogger( FileUtils.class.getName() )
                            .log( Level.SEVERE, null, ex );
                    showError( FileUtils.class.getName(), ex.getMessage() );
                }
                
            });
            
            /**
             * Listener: Shows all of Transactions for an account in a table
             * with a selected data
             */
            transactionDate.setOnAction( ( ActionEvent e ) -> {
                
                try {
                    
                    if( cmbFilter.getValue() != null &&
                        cmbFilter.getValue().trim().equals( "Date" ) ) {
                    
                        filterTransactions();

                    }
                    
                } catch ( IOException | NullPointerException ex ) {
                    
                    Logger
                            .getLogger( FileUtils.class.getName() )
                            .log( Level.SEVERE, null, ex );
                    showError( FileUtils.class.getName(), ex.getMessage() );
                    
                }
                
            });
            
            /**
             * Listener: Shows all of Transactions for an account in a combo box and a table
             * with a wrote description
             */
            transactionDescription.setOnAction( ( ActionEvent e ) -> {
                
                try {
                    
                    if( cmbFilter.getValue() != null &&
                        cmbFilter.getValue().trim().equals( "Description" ) ) {
                    
                        filterTransactions();

                    }
                    
                } catch ( IOException | NullPointerException ex ) {
                    
                    Logger
                            .getLogger( FileUtils.class.getName() )
                            .log( Level.SEVERE, null, ex );
                    showError( FileUtils.class.getName(), ex.getMessage() );
                    
                }
                
            });
            /**
             * Listener: Shows all of Transactions for an account in a combo box and a table
             * with a wrote amount
             */
            transactionAmount.setOnAction( ( ActionEvent e ) -> {
                
                try {
                    
                    if( cmbFilter.getValue() != null &&
                        cmbFilter.getValue().trim().equals( "Quantite" )) {
                    
                        filterTransactions();

                    }
                    
                } catch ( IOException | NullPointerException ex ) {
                    
                    Logger
                            .getLogger( FileUtils.class.getName() )
                            .log( Level.SEVERE, null, ex );
                    showError( FileUtils.class.getName(), ex.getMessage() );
                    
                }
                
            });
            
            /**
             * Shows all of Accounts in a combo box
             */
            listAccounts = loadAccounts();
            cmbAccount.getItems().addAll( listAccounts );

            /**
             * Listener: Shows all of Transactions for an account in a combo box and a table
             */
            cmbAccount.setOnAction((ActionEvent e) -> {
                
                try {
                    
                    listTransactions = loadTransactionsForAccount( cmbAccount.getValue(), null, null );

                    createTable();
                    
                    btnTransactionAdd.setDisable( false );
                    btnTransactionChart.setDisable( false );
                    transactionDate.setDisable( false );
                    transactionDescription.setDisable( false );
                    transactionAmount.setDisable( false );
                    cmbFilter.setDisable( false );
                    
                } catch ( IOException | NullPointerException ex ) {
                    
                    Logger
                            .getLogger( FileUtils.class.getName() )
                            .log( Level.SEVERE, null, ex );
                    showError( FileUtils.class.getName(), ex.getMessage() );
                    
                }
            });
            
            /**
             * Listener: Save a new account with form data
             */
            btnAccountAdd.setOnAction( ( ActionEvent e ) -> {
                
                saveNewAccount();
                
            });
            
            /**
             * Listener: Save a new transaction with form data
             */
            btnTransactionAdd.setOnAction( ( ActionEvent e ) -> {
                if( cmbAccount.getValue() == null || cmbAccount.getValue().toString().trim().equals( "" ) ) {
                    
                    showError( "Erreur!", "Un compte doit être sélectionné." );

                }else if( transactionDate.getValue() == null || transactionDate.getValue().toString().trim().equals( "" ) ){

                    showError( "Erreur!", "La date de la transaction est requis." );

                }else if( transactionDescription.getText().trim().equals( "" ) ){

                    showError( "Erreur!", "La description de la transaction est requis." );

                }else if( transactionAmount.getText().trim().equals( "" ) ){

                    showError( "Erreur!", "La Transaction de la quantite est requis." );

                }else{
                    
                    saveNewTransaction( cmbAccount.getValue(), transactionDate, transactionDescription.getText().trim(), Float.parseFloat( transactionAmount.getText().trim() ) );
                    
                }
                
            });
            
            btnTransactionChart.setOnAction( ( ActionEvent e ) -> {

                try{
                    
                    ObservableList<PieChart.Data> pieChartDataPositive = FXCollections.observableArrayList();
                    ObservableList<PieChart.Data> pieChartDataNegative = FXCollections.observableArrayList();
                    HashMap<String,Float> hashChart = new HashMap<>();

                    listTransactions.forEach( transaction -> {
                        
                        if( !hashChart.isEmpty() &&
                            hashChart.containsKey( transaction.getDescription() ) 
                        ){
                            
                            hashChart.replace( transaction.getDescription() , hashChart.get( transaction.getDescription() ) , hashChart.get( transaction.getDescription() ) + transaction.getquantite() );
                           
                        }else{
                            
                            hashChart.put(transaction.getDescription(), transaction.getquantite() );
                            
                        }

                    });
                    
                    hashChart.forEach( ( keyHash, valueHash ) -> {
                        
                        float amount = valueHash;
                        if( amount < 0 ){ 
                            amount = amount * -1; 
                            pieChartDataNegative.add( new PieChart.Data( keyHash, amount ) );
                        }else{
                            pieChartDataPositive.add( new PieChart.Data( keyHash, amount ) );
                        }
                        

                    });
                    blanceChartPositive.setTitle( "Revenue" );
                    blanceChartNegative.setTitle( "Dépenses" );
                    blanceChartPositive.setData(pieChartDataPositive);
                    blanceChartNegative.setData(pieChartDataNegative);
                    
                }catch(NullPointerException ex){
                    Logger
                    .getLogger( FileUtils.class.getName() )
                    .log( Level.SEVERE, null, e );
                    showError( FileUtils.class.getName(), ex.getMessage() );
                }
            });
            
        }catch( FileNotFoundException e ){
            
            Logger
            .getLogger( FileUtils.class.getName() )
            .log( Level.SEVERE, null, e );
            showError( FileUtils.class.getName(), e.getMessage() );
                    
        }catch( IOException | NullPointerException e ){
            
            Logger
            .getLogger( FileUtils.class.getName() )
            .log( Level.SEVERE, null, e );
            showError( FileUtils.class.getName(), e.getMessage() );
            
        }
    }    
    
    /**
     * Create a Table with a transaction's list
     */
    private void createTable(){

        tableTransactions.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );
        TableColumn<Transaction, String> dateColumn = new TableColumn( "Date" );
        dateColumn.setPrefWidth( 113.2 );
        dateColumn.setSortable( true );
        dateColumn.setResizable( false );
        dateColumn.setCellValueFactory(
            new PropertyValueFactory( "date" )
        );
        TableColumn<Transaction, Float> descriptionColumn = new TableColumn( "Description" );
        descriptionColumn.setPrefWidth( 339.4 );
        descriptionColumn.setSortable( true );
        descriptionColumn.setResizable( false );
        descriptionColumn.setCellValueFactory(
            new PropertyValueFactory( "description" )
        );
        TableColumn amountColumn = new TableColumn( "quantité" );
        amountColumn.setPrefWidth( 113.2 );
        amountColumn.setSortable( true );
        amountColumn.setResizable( false );
        amountColumn.setCellValueFactory(
            new PropertyValueFactory( "quantité" )
        );
        

        tableTransactions.getColumns().clear();
        tableTransactions.getItems().clear();
        tableTransactions.getColumns().addAll( dateColumn, descriptionColumn, amountColumn );
        tableTransactions.getItems().addAll( listTransactions );
        
        // Set new balance
        totalSalary.setText( FileUtils.getTotalAmount( listTransactions ).toString() + "€" );
        
        // Set background colors in case negative or positive amount
        amountColumn.setCellFactory(e -> new TableCell<Transaction, Float>() {
            
            @Override
            public void updateItem(Float item, boolean empty) {
                
                // Always invoke super constructor.
                super.updateItem(item, empty);

                if (item == null || empty) {
                    
                    setText(null);
                    
                } else {
                    
                    setText(item.toString());
                   
                    if(item >= 0 ){
                     
                        this.setStyle( "-fx-text-fill: white; -fx-background-color: green; -fx-alignment: center;" );
                    
                    }else{
                        
                        this.setStyle( "-fx-text-fill: white; -fx-background-color: red; -fx-alignment: center-right;" );
                    
                    }
                    
                }
            }
            
        });
        
        // Cancel transactions with positive amount when row is clicked
        // with secondary button
        tableTransactions.setRowFactory(tv -> {
            TableRow<Transaction> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if ( !row.isEmpty() && 
                    event.getButton()==MouseButton.SECONDARY ) {

                    Transaction clickedRow = row.getItem();
                    System.out.println( clickedRow );
                    if( clickedRow.getquantite() > 0 ){
                        
                        try {
                            List<Transaction> searchCanels = loadTransactionsForAccount( cmbAccount.getValue(), "description", "annulé: " + clickedRow.getDescription() );
                            
                            if( searchCanels.isEmpty() ){
                                
                                if( showConfirmMessage( "Voulez-vous vraiment annulé cette transaction?", "Quantite annulé a sa quantité: " + clickedRow.getquantite() ) ){
                                    
                                    saveNewTransaction( cmbAccount.getValue(), new DatePicker( LocalDate.now() ), "Annulé: " + clickedRow.getDescription(), clickedRow.getquantite() * -1 );
                                    
                                }
                                
                            }else{
                                
                                showMessage( "Transaction precedente annulé", "Cette transaction a été annulé avant." );
                                
                            }
                        } catch ( IOException | NullPointerException ex ) {

                            Logger
                                .getLogger( FileUtils.class.getName() )
                                .log( Level.SEVERE, null, ex );
                            showError( FileUtils.class.getName(), ex.getMessage() );

                        }
                    }
                    
                }
            });
            return row ;
        });
        
    }
    
    /**
     * Save a new account with form data in DB
     */
    private void saveNewAccount(){
        
        if( accountNumber.getText().trim().equals( "" ) ){
                    
            showError( "Error!", "Le numero de compte est requis." );

        }else if( owner.getText().trim().equals( "" ) ){

            showError( "Erreur!", "Le propriétaire est requis." );

        }else{

            try {
                Compte newAccount = new Compte(
                        accountNumber.getText(),
                        owner.getText()
                );
                listAccounts.add(
                        newAccount
                );
                saveAccounts( listAccounts );
                cmbAccount.getItems().add( newAccount );
                showMessage( "Success!", "Le nouveau compte a été enregistré" );

            } catch ( IOException | NullPointerException ex ) {

                Logger
                        .getLogger( FileUtils.class.getName() )
                        .log( Level.SEVERE, null, ex );
                showError( FileUtils.class.getName(), ex.getMessage() );

            }

        }
        
    }
    
    /**
     * Save a new transaction with form data in DB
     */
    private void saveNewTransaction( Compte account, DatePicker date, String description, float amount ){

        try {

            Instant instant = Instant.from(
                    date.getValue().atStartOfDay( ZoneId.systemDefault() )
            );

            Transaction newTransaction = new Transaction(
                    account.getNumero_Compte(),
                    Date.from( instant ),
                    description,
                    amount
            );

            listTransactions.add(
                    newTransaction
            );
            saveTransactions( listTransactions );
            showMessage( "Succes!", "Le nouveau compte a été enregistré." );

            createTable();

        } catch ( IOException | NullPointerException | NumberFormatException ex ) {

            Logger
                    .getLogger( FileUtils.class.getName() )
                    .log( Level.SEVERE, null, ex );
            showError( FileUtils.class.getName(), ex.getMessage() );

        }
        
    }
    
    /**
     * Filter transactions with combo box and inputs 
     * @throws IOException
     * @throws NullPointerException 
     */
    private void filterTransactions() throws IOException, NullPointerException{
        switch( cmbFilter.getValue() ){
            case "Date":

                if( transactionDate.getValue() == null ||
                    transactionDate.getValue().toString().trim().equals( "" ) ){

                    showError( "Erreur!", "La date doit être sélectionné." );

                }else{

                    DateFormat dateFormat = new SimpleDateFormat( "dd/MM/yyyy" );
                    String date = dateFormat.format( 
                        Date.from( 
                            Instant.from(
                                transactionDate
                                .getValue()
                                .atStartOfDay(
                                    ZoneId.systemDefault()
                                )
                            ) 
                        ) 
                    );
                    
                    listTransactions = loadTransactionsForAccount( cmbAccount.getValue(), "date", date );
                
                }
                break;
            case "Description":

                if( transactionDescription.getText().trim().equals( "" ) ){

                    showError( "Erreur!", "La description doit être écrite." );

                }else{

                    listTransactions = loadTransactionsForAccount( cmbAccount.getValue(), "description", transactionDescription.getText() );

                }
                break;

            case "quantite":

                if( transactionAmount.getText().trim().equals( "" ) ){

                    showError( "Erreur!", "Une quantite doit être écrite." );

                }else{

                    listTransactions = loadTransactionsForAccount( cmbAccount.getValue(), "quantite", transactionAmount.getText() );

                }
                break;

            case "Revenue":

                listTransactions = loadTransactionsForAccount( cmbAccount.getValue(), "+", "" );

                break;

            case "Depense":

                listTransactions = loadTransactionsForAccount( cmbAccount.getValue(), "-", "" );

                break;
            default:

                listTransactions = loadTransactionsForAccount( cmbAccount.getValue(), null, null );

                break;
        }
        
        createTable();
        
    }
    
}
